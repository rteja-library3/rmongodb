package rmongodb_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	mongodb "gitlab.com/rteja-library3/rmongodb"
	"go.mongodb.org/mongo-driver/mongo"
)

func Test_StartTransaction(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})
	client.Connect(context.Background())

	session, _ := client.StartSession()

	err := session.StartTransaction()

	assert.NoError(t, err, "[Test_StartTransaction] Should not error")
}

func Test_AbortTransaction(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})
	client.Connect(context.Background())

	session, _ := client.StartSession()
	session.StartTransaction()

	err := session.AbortTransaction(context.Background())

	assert.NoError(t, err, "[Test_AbortTransaction] Should not error")
}

func Test_CommitTransaction(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})
	client.Connect(context.Background())

	session, _ := client.StartSession()
	session.StartTransaction()

	err := session.CommitTransaction(context.Background())

	assert.NoError(t, err, "[Test_CommitTransaction] Should not error")
}

func Test_EndSession(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})
	client.Connect(context.Background())

	session, _ := client.StartSession()
	session.StartTransaction()

	session.EndSession(context.Background())

}

func Test_ID(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})
	client.Connect(context.Background())

	session, _ := client.StartSession()
	session.StartTransaction()

	res := session.ID()

	assert.NotEmpty(t, res, "[Test_ID] Should not empty")
}

func Test_WithTransaction(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})
	client.Connect(context.Background())

	session, _ := client.StartSession()
	session.StartTransaction()

	_, res := session.WithTransaction(context.Background(), func(sessCtx mongo.SessionContext) (interface{}, error) {
		return nil, nil
	})

	assert.NotEmpty(t, res, "[Test_WithTransaction] Should not empty")
}
