package rmongodb_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	mongodb "gitlab.com/rteja-library3/rmongodb"
	"go.mongodb.org/mongo-driver/mongo"
)

func Test_ClientAdapter_Connect(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})

	err := client.Connect(context.Background())

	assert.NoError(t, err, "Should not error")
}

func Test_ClientAdapter_Disconnect(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})

	err := client.Disconnect(context.Background())

	assert.NoError(t, err, "Should not error")
}

func Test_ClientAdapter_Database(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})

	db := client.Database("test")

	assert.NotNil(t, db, "Should not nill")
}

func Test_ClientAdapter_StartSession(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})
	client.Connect(context.Background())

	session, _ := client.StartSession()

	assert.NotNil(t, session, "Session should nil")
}

func Test_ClientAdapter_StartSession_Err(t *testing.T) {
	client := mongodb.NewClientAdapter(&mongo.Client{})

	session, err := client.StartSession()

	assert.Error(t, err, "Should error")
	assert.Equal(t, "client is disconnected", err.Error(), "Error should \"client is disconnected\"")
	assert.Nil(t, session, "Session should nil")
}
