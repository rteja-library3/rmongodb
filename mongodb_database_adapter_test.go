package rmongodb_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	mongodb "gitlab.com/rteja-library3/rmongodb"
	"go.mongodb.org/mongo-driver/mongo"
)

func Test_MongoDatabaseAdapter_Collection(t *testing.T) {
	mca := mongodb.NewClientAdapter(&mongo.Client{})

	db := mca.Database("db-test")

	c := db.Collection("test")

	assert.NotNil(t, c, "Collection should not be nil")
}
